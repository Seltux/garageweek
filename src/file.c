#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "../lib/cigma.h"
#include "../lib/consoletools.h"
#include "../lib/Cwing.h"
#include "../src/file.h"
#include "../lib/KToolsLib.h"

int carac_ok(char c){
  int i;
  if(c >= '0' && c<= '9')
  i = 1;
  else if (c>= 'a' && c<= 'z')
  i=1;
  else if (c>= 'A' && c<= 'Z')
  i=1;
  else if (c== '/')
  i=1;
  else i=0;

  return i;
}

void parse_file(FILE *f, char parser, char* dest){
  char c;
  char s[2];
  s[0] = '\0';
  dest[0] = '\0';
  c=fgetc(f);
  while(c != parser){

    if(c != parser && carac_ok(c) == 1){
      s[0] = c;
      s[1] = '\0';
      strcat(dest, s);
    }
    c=fgetc(f);
  }
}

void write_file(char* name, DropMenu ptr,int NumeroChambre, Slider Echelle, Point_2f painZone){
    FILE *f;

    time_t intps;
    struct tm * p_dateheure;
    intps = time(NULL);
    p_dateheure = localtime(&intps);
    int sec = p_dateheure->tm_sec;
    int min = p_dateheure->tm_min;
    int hour = p_dateheure->tm_hour;

    char* dp = str_format("%s",ptr.selected_option);
    int value = (int)Echelle.value;
    float X = painZone.x;
    float Y = painZone.y;

    f= fopen(name,"a+");
    if(f==NULL){
        printf("ERROR : NULL");
    }
    else{
        fprintf(f,"%d:%d:%d",hour,min,sec);
        fprintf(f,";");
        fprintf(f,"%d",NumeroChambre);
        fprintf(f,";");
        fprintf(f,"%s",dp);
        fprintf(f,";");
        fprintf(f,"%d",value);
        fprintf(f,";");
        fprintf(f,"%f",X);
        fprintf(f,";");
        fprintf(f,"%f\n",Y);
    }
    fclose(f);
}

void* function_sauvegarde(char* name, DropMenu ptr,int NumeroChmabre, Slider Echelle, Point_2f painZone){
    write_file(name, ptr, NumeroChmabre, Echelle,painZone);
    return 0;

}