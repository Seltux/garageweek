#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "../lib/cigma.h"
#include "../lib/consoletools.h"
#include "../lib/Cwing.h"
#include"../src/file.h"

void initialize ();
void update ();
void render ();


int main(int argc,char** argv){
	int largeurFenetre 	   = 700;
	int hauteurFenetre 	   = 450;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "HandiTab";
	int limFPS 			   = 1000;
	void (*callbacks[6])() = {initialize,update,render,NULL,NULL,NULL};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}


///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////

#define NUM_CHAMBRE 302

char* styles = "../resources/style.css";

FontText* myfont;
FontText* titlefont;
DropMenu dm_pb;
Button bt_valider;
Slider sl_intensity;
Image* imgcorps;
Point_2f pain;

void actionOfValider (){
	function_sauvegarde("DOSSIER-PATIENT.txt",dm_pb,NUM_CHAMBRE,sl_intensity,pain);
}

//fonction d'initialisation Cigma
void initialize (){
	dm_pb = new_DropMenuCSS(styles,"dropy");
	dm_pb.add(&dm_pb,"Manger");
	dm_pb.add(&dm_pb,"Soif");
	dm_pb.add(&dm_pb,"Douleur");
	dm_pb.add(&dm_pb,"Toilette");
	dm_pb.selected_option="Manger";
	bt_valider = new_Button_CSS("Valider",styles,"buttons");
	bt_valider.addEventListener(&bt_valider,"click",actionOfValider);
	sl_intensity = new_Slider_CSS(0,10,5,styles,"slide");
	myfont = makeVectorialFont(GLUT_STROKE_ROMAN,100);	//new_FontText("../resources/fonts/roboto.ttf",100);
	titlefont = new_FontText("../resources/fonts/police.ttf",100);
	imgcorps = new_Image("../resources/corps.png",true);
	pain = new_Point_2f(0,0);
}

//fonction de rafraichissement Cigma
void update (){
	dm_pb.update(&dm_pb);
	bt_valider.update(&bt_valider);
	sl_intensity.update(&sl_intensity);

	ifleftclickup(){
		int x = 3*LF/5+(LF-3*LF/5)/2-imgcorps->static_width/2, y = 0.38*HF;
		if(pointIsInRectangle(AS,OS,x,y,imgcorps->static_width,imgcorps->static_height)){
			pain.x = (AS-x)/imgcorps->static_width;
			pain.y = (OS-y)/imgcorps->static_height;
		}
	}
	

}

//fonction d'affichage Cigma
void render (){
	clear(50,50,50);
	int xg = 20;
	int xd = 3*LF/5;
	strokeRGB(COLOR_BLUE_VIOLET);
	int txth =0.09*HF < 90 ? 0.09*HF:90;
	text(titlefont,xd/2-getTextWidth(titlefont,txth,"HandiTab")/2,0.85*HF,txth,"HandiTab");
	strokeRGB(COLOR_GHOST_WHITE);

	// ZONE GAUCHE
	fitTextToRectangle(myfont,xg,3./4*HF,1./5*LF,30,"Probleme :");
	fitTextToRectangle(myfont,xg,2./4*HF,1./5*LF,30,"Intensite :");
	bt_valider.show(&bt_valider,xd/2-ratioValue(bt_valider.style.width,LF)/2,1./4*HF);
	sl_intensity.show(&sl_intensity,1./5*LF+xg+15,2./4*HF+10);
	dm_pb.show(&dm_pb,1./5*LF + xg+15,3./4*HF);

	// ZONE DROITE :
	strokeRGB(COLOR_GHOST_WHITE);
	line(xd,0.38*HF,xd,0.95*HF);
	fitImageToRectangle(imgcorps,LF-xd,(0.95-0.38)*HF);
	image(imgcorps,xd+(LF-xd)/2-imgcorps->static_width/2,0.38*HF);
	if(pain.x != 0 || pain.y != 0){
		strokeRGBA(RGBtoRGBA(COLOR_RED,100));
		strokeWidth(0.2*imgcorps->static_width);
		dot(xd+(LF-xd)/2-imgcorps->static_width/2 + pain.x*imgcorps->static_width, 0.38*HF + pain.y*imgcorps->static_height);
	}

	// PANEL INFIRMIERE
	int xinfi = xd+20, yinfi = 20, winfi = LF-xd-40, hinfi = 0.38*HF-40;
	strokeRGB(COLOR_GHOST_WHITE);
	fillRGB(COLOR_GREY);
	strokeWidth(3);
	roundedRectangle(xinfi,yinfi,winfi,hinfi,0.3,40);
	//fitTextToRectangle(myfont,xd,230,FONT_SIZE,"Infirmiere");
	fitTextToRectangle(myfont,xinfi,yinfi + 2./3*hinfi,	winfi,	1./5*hinfi,"En Attente");
	fitTextToRectangle(myfont,xinfi,yinfi + 1./3*hinfi,	winfi,	1./5*hinfi,"En Route");
	fitTextToRectangle(myfont,xinfi,yinfi,				winfi,	1./5*hinfi,"Arrivee");
	fitTextToRectangle(myfont,xd,0.95*HF,LF-xd,0.05*HF,"Zone du corps");

	displayFPS(3);
}






