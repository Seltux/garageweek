#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "../lib/cigma.h"
#include "../lib/consoletools.h"
#include "../lib/Cwing.h"
#include "../lib/KToolsLib.h"

void initialize ();
void update ();
void render ();


int main(int argc,char** argv){
	int largeurFenetre 	   = 700;
	int hauteurFenetre 	   = 450;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "HandiTab";
	int limFPS 			   = 1000;
	void (*callbacks[6])() = {initialize,update,render,NULL,NULL,NULL};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}


///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////

class(TimeHour){
	int h,m,s;
};

class(Demande){
	TimeHour time;
	Point_2f pain;
	char* pb;
	int room;
	int intensity;
};

char* styles = "../resources/style.css";
Demande** list;
FontText* myfont;
Image* img_dou;
Image* img_fai;
Image* img_soi;
Image* img_toi;
int scrolloffset = 0;

TimeHour new_TimeHour (int h,int m,int s){
	return (TimeHour) {h,m,s};
}

Demande new_Demande (TimeHour time,Point_2f pain,char* pb,int room,int intensity){
	Demande dem;
	dem.pain = pain;
	dem.time = time;
	dem.pb = str_format("%s",pb);
	dem.room = room;
	dem.intensity = intensity;
	return dem;
}

char* DemandeToString (Demande dem){
	return str_format("%d:%d:%d;%d;%s;%d;%f;%f",dem.time.h,dem.time.m,dem.time.s,dem.room,dem.pb,dem.intensity,dem.pain.x,dem.pain.y);
}

void showDemande (Demande dem,int x,int y,int width,int height){
	strokeRGB(COLOR_GREY);
	fillRGB(COLOR_WHITE);
	strokeWidth(3);
	roundedRectangle(x,y,width,height,0.2,50);
	if(str_same(dem.pb,"Douleur")){
		imageCustom(img_dou,x+20,y+20,height-40,height-40);
	}
	if(str_same(dem.pb,"Manger")){
		imageCustom(img_fai,x+20,y+20,height-40,height-40);
	}
	if(str_same(dem.pb,"Soif")){
		imageCustom(img_soi,x+20,y+20,height-40,height-40);
	}
	if(str_same(dem.pb,"Toilette")){
		imageCustom(img_toi,x+20,y+20,height-40,height-40);
	}
	text(myfont,x+width/2-100,y+3./4*height,23,"Chambre   : %d",dem.room);
	text(myfont,x+width/2-100,y+2./4*height,23,"Intensite : %d",dem.intensity);
	text(myfont,x+width/2-100,y+1./4*height,23,"Depuis    : %d:%d:%d",dem.time.h,dem.time.m,dem.time.s);
}

void addDemande(Demande dem){
	int nb = count(list);
	list = realloc(list,sizeof(Demande)*(nb+2));
	list[nb] = malloc(sizeof(Demande));
	*(list[nb]) = dem;
	list[nb+1] = NULL;
}

void removeDemande(int index){
	for(int i = index;list[i] != NULL;i++){
		list[i] = list[i+1];
	}
}


//fonction d'initialisation Cigma
void initialize (){
	img_dou = new_Image("../resources/pain.png",true);
	img_fai = new_Image("../resources/food.png",true);
	img_soi = new_Image("../resources/thirst.png",true);
	img_toi = new_Image("../resources/toilet.png",true);
	list = malloc(sizeof(Demande));
	list[0] = NULL;
	myfont = new_FontText("../resources/fonts/Consolas.ttf",100);
	Demande dem1 = new_Demande(new_TimeHour(12,30,23),new_Point_2f(0.3,0.5),"Douleur",303,1);
	Demande dem2 = new_Demande(new_TimeHour(12,30,23),new_Point_2f(0.3,0.5),"Manger",303,2);
	Demande dem3 = new_Demande(new_TimeHour(12,30,23),new_Point_2f(0.3,0.5),"Soif",323,3);
	addDemande(dem1);
	addDemande(dem2);
	addDemande(dem3);
}

//fonction de rafraichissement Cigma
void update (){
	float scrollspeed = 17;
	ifscrolldown(){
		scrolloffset += scrollspeed;
	}
	ifscrollup(){
		scrolloffset = scrolloffset-scrollspeed < 0 ? 0 : scrolloffset-scrollspeed;
	}
}

//fonction d'affichage Cigma
void render (){
	clear(90,90,90);
	for(int i=0; i<count(list);i++){
		showDemande(*(list[i]),0,HF - (i+1)*(1./5*HF+15)+scrolloffset,LF,1./5*HF);
	}
}






