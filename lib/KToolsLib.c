#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>

#include "consoletools.h"
#include "KToolsLib.h"


bool str_same (char* str1,char* str2){
	return !strcmp(str1,str2);
}

int str_substringIndex (char* source,char* substr){
	int cpt = 0;
	for(int i=0;i<strlen(source);i++){
		if(source[i] == substr[cpt])
			cpt++;
		else 
			cpt = 0;
		if(cpt == strlen(substr))
			return i - strlen(substr) + 1;
	}
	return -1;
}

float randFloat (){
	static bool firstcall = true;
	if(firstcall){
		srand((unsigned int)time(NULL));
		firstcall = false;
	}
    return (float)rand()/(float)(RAND_MAX);
}

char* fileGetExtension (char* path){
	int first;
	for(first = strlen(path)-1;first >= 0 && path[first] != '.';first--);
	int size = 1;
	char* ret = malloc(sizeof(char));
	ret[0] = '\0';
	for(int i = first+1;i< strlen(path);i++){
		size++;
		ret = realloc(ret,sizeof(char)*size);
		ret[size-2] = path[i];
		ret[size-1] = '\0';
	}
	return ret;
}

char* runProcess (char* command,char* logs_location){
	system(command);

	FILE* logs = fopen(logs_location,"rt");
	if(logs != NULL){
		while(logs == NULL || strlen(fileGetText(logs_location)) < 3){
			fclose(logs);
			logs = fopen(logs_location,"rt");
		}
		fclose(logs);
		char* fcontents = fileGetText(logs_location);
		
		system(str_format("rm \"%s\"",logs_location));
		return fcontents;
	}else{
		return NULL;
	}
}

int parseInt (char* str){
	return atoi(str);
}

float parseFloat (char* str){
	return atof(str);
}

double min (double a,double b){
	return (a>b)? b : a;
}

double max (double a,double b){
	return (a>b)? a : b;
}

char* str_format (char* format,...){
	va_list arg;
  	va_start (arg, format);
	char* ret = malloc(sizeof(char)*1000);
  	vsprintf (ret, format, arg);
  	va_end (arg);
	return ret;
}

bool isValInArray (void** tab,void* val){
	foreach(void** tmp,tab){
		if(*tmp == val){
			return true;
		}
	}
	return false;
}

char** str_split(char* a_str, const char a_delim){
	DEBUGSTART;
	printdebug("args = %s,%c",a_str,a_delim);
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;
    while (*tmp){
        if (a_delim == *tmp){
            count++;
            last_comma = tmp;
        }
        tmp++;
    }
    count += last_comma < (a_str + strlen(a_str) - 1);
    count++;
    result = malloc(sizeof(char*) * (count+1));
    if (result){
        size_t idx  = 0;
        char* token = strtok(a_str, delim);
        while (token){
            //assert(idx < count);
            result[idx++] = strdup(token);
            token = strtok(0, delim);
        }
        //assert(idx == count - 1);
        result[idx] = 0;
    }
	result[count] = false;
	DEBUGEND;
    return result;
}

#define DBSIZE 16000

//TODO-N Auto malloc de fcontents 
//TODO-N ne plus utiliser DBSIZE mais la taille du fichier 
int getDbText (char* fcontents,char* filename){
	DEBUGSTART;
	size_t nread;
	FILE* f = fopen(filename,"r");
	if( f!= NULL){
		do{
			nread = fread(fcontents,1,DBSIZE*sizeof(char),f);
		}
		while(nread > 0);
		fclose(f);
		return 0;
	}
	else{
		//printf(TXT_RED);
		printf("ERROR : FILE \"%s\" NOT FOUND\n",filename);
		//printf(TXT_END_COLOR);
		return 1;
	}
	DEBUGEND;
}

char* fileGetText (char* filename){
	DEBUGSTART;
	FILE* f = fopen(filename,"r");
	if( f!= NULL){
		fseek (f, 0, SEEK_END);
		double length = ftell (f);
		fseek (f, 0, SEEK_SET);
		char* str = malloc (length+1);
		if (str){
			fread (str, 1, length, f);
		}
		fclose(f);
		int end = length;
		str[end] = '\0';
		return str;
	}
	else{
		//printf(TXT_RED);
		printf("ERROR : FILE \"%s\" NOT FOUND\n",filename);
		//printf(TXT_END_COLOR);
		return NULL;
	}
	DEBUGEND;
}

int count2 (void** T){
	double** A = (double**) T;
	int i = 0;
	while(A[i]){
		i++;
	}
	return i;
}

//TODO-I Corriger erreur buffer overflow en fin de ligne voir mariage.csv derniere ligne du a '\n' ?
//TODO-N Rendre fonction generique
//CORRIGER ';' en fin de ligne
char* str_replaceAll(char* src/*,char* target,char* replacement*/){
	DEBUGSTART;
	char* ret = malloc(sizeof(char));
	int size = 1;
	int cpt = 0;
	for(int i=0;src[i]!='\0';i++){
		cpt = (src[i] == ';') ? cpt+1 : 0;
		if(cpt == 2){
			size+=2;
			ret = realloc(ret,sizeof(char)*size);
			ret[size-2] = ' ';
			ret[size-1] = ';';
			cpt = 1;
		}
		else{
			size+=1;
			ret = realloc(ret,sizeof(char)*size);
			ret[size-1] = src[i];
		}
	}
	size++;
	ret = realloc(ret,sizeof(char)*size);
	ret[size-1] = '\0';
	DEBUGEND;
	return ret;
}

char* str_removeAll(char* src,char target){
	DEBUGSTART;
	char* ret = malloc(sizeof(char));
	*ret = '\0';
	int size = 1;
	for(int i=0;i < strlen(src);i++){
		if(src[i] != target){
			ret = realloc(ret,sizeof(char)*(size+1));
			ret[size-1] = src[i];
			ret[size] = '\0';
			size++;
		}
		/*else{
			println("%d",i);
		}*/
	}
	DEBUGEND;
	return ret;
}

//retourne le nombre d elements initialisés dans un tableau bidimensionnel d entier
int trueSizeOfIntTab (int **T){
	int a;
	for(a=1;T[a][0] != -1;a++){
	}
	return a;
}
//cree par K MARIE

//retourne le nombre d elements initialisés dans un tableau d entier
int trueSizeOfInt (int *T){
	int a;
	for(a=0;T[a] != -1;a++){
	}
	return a;
}
//cree par K MARIE

//retourne true si un fichier donné existe
bool fileExist (char* nomFichier){
	bool a = false;
	FILE* f = fopen(nomFichier,"rt");
	if(f==NULL){a=false;}
	if(f!=NULL){a=true;fclose(f);}
	return(a);
}
//cree par K MARIE

//fclose seulement si le fichier != NULL
void betterfclose (FILE* f){
	if(f==NULL){}
	if(f!=NULL){fclose(f);}
}
//cree par K MARIE

//free seulement si le pointeur != NULL
void betterFree2 (void **ptr){
	if(*ptr!=NULL){free(*ptr);*ptr=NULL;}
	if(*ptr!=NULL){}
}
//cree par K MARIE

//permet de malloc un tableau a deux dimensions
void** bigMalloc (int size,int DIM1,int DIM2){
	void** ptr = (void**) malloc(sizeof(void*)*DIM1);
	for(int i=0;i<DIM1;i++){
		ptr[i] = (void*) malloc(size*DIM2);
	}
	return ptr;
}
//cree par K MARIE

//permet de generer une chaine contenant un chemin vers un fichier donné avec un nombre aleatoire
char* randomPath(int nbFichier,char* path,char* extension){
	char* ch;
	char* a;
	float f = 11.;
	int i;
	int nbAleatoire = nbFichier*randFloat();
	for(i = 0;f>10.;i++){				//i est le nombre de caracteres necessaire pour stocker l'entier sous forme de chaine
		f =(float) nbFichier/(pow(10,i));
	}
	ch = (char*) malloc(sizeof(char)*(1+i) + sizeof(path) + sizeof(extension));
	a = (char*) malloc(sizeof(char)*(1+i));
	strcpy(ch,path);
	sprintf(a,"%d",nbAleatoire);
	strcat(ch,a);
	strcat(ch,extension);
	return ch;
} 
//cree par K MARIE

bool not (bool a){
	if(a == true){
		return false;
	}
	else{
		return true;
	}
}
