#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include "../lib/KToolsLib.h"
#include "consoletools.h"
#include "string.h"

//activer l'affichage des printdebug() ?
bool debug_mode = true;
//activer l'affichage des printerr() ?
bool ignore_error = false;

//activer l'affichage des printdebug() ?
void setDebugMode (bool val){
    debug_mode = val;
}

//activer l'affichage des printerr() ?
void setIgnoreError (bool val){
    ignore_error = val;
}

//accesseur du mode debug
bool getDebugMode (){
    return debug_mode;
}

//accesseur du mode ignoreError
bool getIgnoreError (){
    return ignore_error;
}

/**
 * @brief affichage sur la console d'une chaine de caractere colorée
 * 
 * @param color couleur d affichage valeurs possibles :
	TXT_COLOR_YELLOW
	TXT_COLOR_BLACK
	TXT_COLOR_GREEN
	TXT_COLOR_PURPLE
	TXT_COLOR_LIGHT_BLUE
	TXT_COLOR_DARK_BLUE
	TXT_COLOR_RED
 * @param format chaine de caractere formatée
 */
void printlncolored (char* color,const char *format, ...){
	printf("%s",color);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en jaune
 * 
 * @param format chaine de caractere formatée
 */
void printyellow (const char *format, ...){
	printf("%s",TXT_COLOR_YELLOW);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en vert
 * 
 * @param format chaine de caractere formatée
 */
void printgreen (const char *format, ...){
	printf("%s",TXT_COLOR_GREEN);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en violet
 * 
 * @param format chaine de caractere formatée
 */
void printpurple (const char *format, ...){
	printf("%s",TXT_COLOR_PURPLE);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en bleu clair
 * 
 * @param format chaine de caractere formatée
 */
void printlightblue (const char *format, ...){
	printf("%s",TXT_COLOR_LIGHT_BLUE);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en bleu foncé
 * 
 * @param format chaine de caractere formatée
 */
void printdarkblue (const char *format, ...){
	printf("%s",TXT_COLOR_DARK_BLUE);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en rouge
 * 
 * @param format chaine de caractere formatée
 */
void printred (const char *format, ...){
	printf("%s",TXT_COLOR_RED);
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("%s",TXT_COLOR_END);
  	va_end (arg);
}

/**
 * @brief affichage sur la console d'une chaine de caractere en sautant une ligne
 * 
 * @param format chaine de caractere formatée
 */
void println (const char *format, ...){
  	va_list arg;
  	va_start (arg, format);
  	vfprintf (stdout, format, arg);
	printf("\n");
  	va_end (arg);
}

/**
 * @brief retourne l'index de la derniere apparition d un carctere donné dans une chaine
 * 
 * @param str chaine de caracteres
 * @param search charactere recherché
 * @return int index
 */
int str_indexOfLast (char* str,char search){
	int i;
	for(i=strlen(str)-1;i>=0 && str[i] != search;i--);
	return i+1;
}

/**
 * @brief retourne une chaine de caractere découpée a partir d'une chaine donnée
 * 
 * @param str chaine
 * @param start index du debut de la coupure
 * @param end index de la fin de la coupure
 * @return char* chaine résultante
 */
char* substring (char* str,int start,int end){
	char* ret = malloc(sizeof(char)*(end-start+1));
	for(int i=start;i<end;i++){
		ret[i-start] = str[i];
	}
	ret[end-start] = '\0';
	return ret;
}

/*
NE PAS UTILISER
utiliser printdebug()
 */
void printdebug2 (char* filename,int line,const char* func,const char *format, ...){
	if(debug_mode){
		char* reduced_filename = substring(filename,str_indexOfLast(filename,'/'),strlen(filename));
        int color = (int)(41+((float)1.*randFloat()*6));
		printf("\x1b[%dm",color);
		printf("%s",TXT_COLOR_BLACK);
		va_list arg;
		va_start (arg, format);
		printf("%s-> %s() @L-%d :%s ",reduced_filename,func,line,TXT_COLOR_END);
		vfprintf (stdout, format, arg);
		printf("\n");
		va_end (arg);
		free(reduced_filename);
		reduced_filename = NULL;
	}
}

/**
 * @brief affichage sur la console d'un message d'erreur en rouge
 * 
 * @param format chaine de caractere formatée
 */
void printerr (const char *format, ...){
	if(!ignore_error){
		printf("%s",TXT_COLOR_RED);
		va_list arg;
		va_start (arg, format);
		vfprintf (stdout, format, arg);
		printf("%s\n",TXT_COLOR_END);
		va_end (arg);
	}
}

/**
 * @brief affiche toutes les couleurs de la console
 * 
 */
void printPalette(){
	for(int i=0;i<50;i++){
		printf("\x1b[%dm %d %s,",i,i,TXT_COLOR_END);
	}
	printf("\n");
}
