// TYPEDEFS
#include "../lib/geometry.h"
#include "../lib/cssparser.h"

#ifndef class
	#define class(x) typedef struct x x; struct x
#endif

typedef Point** Trajectoire;


// Structure bouton clickable
class(Button){
	int x,y;
	char *text;
	bool hover;
	char listener[50];
	int oldwidth,oldheight;
	void (*event) (void);

	Style style;
	Style hover_style;

	/**
	 * @brief ajout d'un evenement a un bouton
	 * 
	 * @param listener type d'evenement activant le bouton ex: "click", "key_a" ...
	 * @param event fonction callback du bouton
	 */
	void (*addEventListener) 	(Button* self,char* listener,void (*event) (void));
	void (*roundBorders) 		(Button* self,float factor);
	void (*show) 				(Button* self,int x,int y);
	void (*update)				(Button* self);
};

Button new_Button		(char* text,int width,int height,void* font);
Button new_Button_CSS	(char* text,char* pathcss,char* classname);

//structure champs de texte
class(Textfield){
	int x,y;
	char *text;
	bool isEditing;

	bool blink;
	int selecZonex; 
	int selecZonew; 
	int cursorpos;
	double t0;
	bool held;
	char* selected_text;

	Style style;
	Style focus_style;
	
	void (*roundBorders) 	(Textfield* self,float factor);
	void (*show) 			(Textfield* self,int x,int y);
	void (*update)			(Textfield* self);
};

Textfield new_Textfield(int width,int height,void* font);
Textfield new_Textfield_CSS(char* csspath,char* classname);

//structure label
class(Label){
	int x,y;
	char *text;

	int selecZonex; 
	int selecZonew; 
	bool held;
	char* selected_text;

	Style style;
	
	void (*show) 			(Label* self,int x,int y);
	void (*update)			(Label* self);
};

Label new_Label(char* text,int width,int height,void* font);
Label new_Label_CSS(char* text,char* csspath,char* classname);

//structure menu clic droit
class(ClickMenu){
	Button* option;
	int* separateurs;
	int nboption;
	int nbseparateurs;
	bool is_shown;

	void (*show) (ClickMenu* self,int x,int y);
	void (*update) (ClickMenu* self);
};

//structure menu déroulant
class(DropMenu){
	int x,y;
	char** option;
	char* selected_option;
	int index_selected_option;
	bool isOpened;
	int index_option_hovered;
	Style style;
	Style style_hover;

	//Methodes

	//ajout d'une option au dropMenu
	void (*add) (DropMenu* self,char* option);
	//retourne l'index de l'option donnée
	int  (*indexOf) (DropMenu* self,char* option);
	//supprime une option du dropmenu
	void (*delete) (DropMenu* self,char* option);
	//affiche le dropmenu
	void (*show) (DropMenu* self,int x,int y);
	//actualise le dropmenu
	void (*update) (DropMenu* self);
	//supprime toutes les options du dropmenu
	void (*deleteAll) (DropMenu* self);
	void (*event) ();
};

DropMenu new_DropMenu (int width,int height);
DropMenu new_DropMenuCSS (char* csspath,char* classname);

//structure permettant de faire varier des coordonées selon une trajectoire
class(Mover){
	bool activated;
	Trajectoire trajectoire;
	int current_pt;
	bool repeat;
	double t0;
	float tps;
	float elapsed_time;
	bool reverse;
	bool reverse_loop;
	bool loop;
	
	int *x;
	int *y;

	void (*activate)(Mover* self);
	void (*deactivate)(Mover* self);
	void (*assign)(Mover* self,int* x,int *y);
	void (*update)(Mover* self);
};

Mover new_Mover (Point** trajectoire,bool repeat,float tps,bool reverse_loop,bool loop);

//structure slider
class(Slider){
	int x,y;
	double min,max;
	double value;
	bool float_mode;
	bool selected;
	Style style;
	void (*callback) (void);

	//definit si le slider accepte les flottants
	void (*setFloatMode) (Slider* self);
	//definit si le slider n accepte que les entiers
	void (*setIntMode) (Slider* self);
	//affichage
	void (*show) (Slider* self,int x,int y);
	//définit le callback a la selection d une option
	void (*setCallback) (Slider* self,void (*callback) (void));
	//actualisation
	void (*update) (Slider* self);
};

Slider new_Slider (double min,double max,double default_value);
Slider new_Slider_CSS (double min,double max,double default_value,char* csspath,char* classname);

//structure selecteur de couleur RGB
class(ColorPicker){
	bool selected;
	RGB selection;
	Image* gradient;
	RGB** indexer;
	int width;
	int height;
	int x,y;
	int xselec,yselec;

	Slider r_slider;
	Slider g_slider;
	Slider b_slider;

	Button validate;

	//actualisation
	void (*update) (ColorPicker* self);
	//affichage
	void (*show) (ColorPicker* self,int x,int y);
	//definit la fonction a appeler lorsque une couleur est selectionée
	void (*setCallback) (ColorPicker* self,void (*function) (void));
};

class(Counter){
	int x,y;
	int value;
	int valmax;
	int valmin;
	Style style;
	void (*eventListener) (void);


	void (*update) (Counter* self);
	void (*show) (Counter* self,int x,int y);
	void (*addEventListener) (Counter* self,void (*func)(void));
};

Counter new_Counter (int valmin,int val,int valmax);
Counter new_Counter_CSS (int valmin,int val,int valmax,char* csspath,char* classname);


class(Overlay){
	Image* icon;
	Style style;
	bool movewindow;
	bool activated;

	void(*update) (Overlay* self);
	void(*show) (Overlay* self);
	void(*activate) (Overlay*self);
};

Overlay new_Overlay(char* imagePathIcon);
Overlay new_Overlay_CSS(char* imagePathIcon,char* csspath,char* classname);